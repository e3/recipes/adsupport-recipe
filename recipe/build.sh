#!/bin/bash

LIBVERSION=${PKG_VERSION}

DEPENDENCIES="WITH_XML2=YES \
XML2_EXTERNAL=NO \
WITH_NETCDF=YES \
NETCDF_EXTERNAL=YES \
WITH_JPEG=YES \
JPEG_EXTERNAL=YES \
WITH_ZLIB=YES \
WITH_BLOSC=YES \
BLOSC_EXTERNAL=YES \
WITH_TIFF0=YES \
TIFF_EXTERNAL=YES \
WITH_SZIP=YES \
WITH_HDF5=YES \
HDF5_EXTERNAL=YES \
WITH_BITSHUFFLE=YES \
WITH_NEXUS=YES"

###########
#
# We need to rename a few source files, as they are all compiled into a single folder O.${EPICS_VERSION}_${T_A}, and some
# names are in common between sub-modules. These files are all temporary (they are linked together in libadsupport.so), so
# renaming the source files seems the simplest solution for the time being.
#
# Note that the Makefile has been altered to account for this, see e.g. the variable CODER_SRCS.
#
###########

bash fixnames.bash supportApp/zlibSrc || exit 1
bash fixnames.bash supportApp/GraphicsMagickSrc/coders || exit 1
bash fixnames.bash supportApp/szipSrc || exit 1
bash fixnames.bash supportApp/xml2Src trionan.c || exit 1


# Clean between variants builds
make clean

make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} ${DEPENDENCIES}
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} ${DEPENDENCIES} db_internal
make MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} ${DEPENDENCIES} install
