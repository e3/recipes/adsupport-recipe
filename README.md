# ADSupport conda recipe

Home: "https://github.com/areaDetector/ADSupport"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADSupport module
